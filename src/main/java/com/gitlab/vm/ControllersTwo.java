package com.gitlab.vm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllersTwo {
    @GetMapping("/videos")
    public String videos() {
        return "videos";
    }
}