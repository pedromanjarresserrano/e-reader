package com.gitlab.vm;


import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.api.impl.FilesStaticResourceLocationsImpl;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.services.StorageService;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

import java.io.IOException;

/**
 * The Class ProfileViewModel.
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ViewVideo {

    private Video video;

    private String source;

    /**
     * Inits the.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Init
    public void init() throws IOException {
        video = (Video) Executions.getCurrent().getDesktop().getSession().getAttribute("video");
        String urlFile = ApplicationContextUtils.getBean(StorageService.class).getUrlFile(video.name());
        String url = FilesStaticResourceLocationsImpl.STATIC_FILES_PATH + video.name();
        String replace = url.replace("\\", "/");

        source = url;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}