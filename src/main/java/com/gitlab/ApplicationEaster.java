package com.gitlab;

import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.VideoService;
import com.gitlab.pedrioko.spring.security.ServletsConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * The Class com.ereader.Application.
 */
@SpringBootApplication
@Configuration

@EntityScan(basePackages = {"com.gitlab"})
@PropertySource("classpath:/applicationeaster.properties")
public class ApplicationEaster {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ApplicationEaster.class).run();

     /*   Object entityByQuery = ApplicationContextUtils.getBean(CrudService.class)
                .getEntityByHQLQuery("select distinct vid from Video vid" +
                        "    left join FETCH vid.categorias as categorias" +
                        "    left join FETCH vid.actrices as actrices" +
                 "    left join FETCH vid.listfiles as listfiles", 0, 30);
        System.out.println(entityByQuery);*/
    }


}


