package com.gitlab.controllers;

import com.gitlab.pedrioko.core.lang.api.RangeValue;
import com.gitlab.pedrioko.core.hibernate.query.Limit;
import com.gitlab.pedrioko.core.hibernate.query.Where;
import com.gitlab.pedrioko.core.view.enums.ParamMode;
import com.gitlab.pedrioko.core.reflection.ReflectionJavaUtil;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.view.viewers.crud.grid.AlphabetFilter;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.StorageService;
import com.google.gson.Gson;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.CollectionPath;
import com.querydsl.core.types.dsl.ComparablePath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

//@RestController("/test")
public class TestController {

    @Autowired
    private StorageService storageService;
    @Autowired
    private CrudService crudService;
    private ParamMode paramMode = ParamMode.AND;

    private Map<String, Object> params = new LinkedHashMap<>();

    @RequestMapping(value = "/tester", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<?> handleRequest(@RequestParam String clase, @RequestBody Where where) throws Exception {
        List<?> entities = ApplicationContextUtils.getEntities();
        List<?> collect = entities.stream().filter(e -> (((Class) e).getSimpleName().equalsIgnoreCase(clase))).collect(Collectors.toList());
        if (!collect.isEmpty()) {
            Object o = collect.get(0);
            params.clear();
            setParam(where, params);
            PathBuilder pathBuilder = crudService.getPathBuilder((Class) o);
            Predicate predicate = getPredicate(pathBuilder, where.getLimit());
            JPAQuery<?> from = crudService.query().from(pathBuilder);
            if (predicate != null) {
                from = from.where(predicate);
            }
            Limit limit = getLimit(where);
            if (limit != null) {
                from = from.offset(limit.getOffset()).limit(limit.getLimit());
            }
            return Mono.justOrEmpty(from.fetch());

        } else {

            return Mono.justOrEmpty(null);
        }

    }

    private Limit getLimit(Where where) {
        if (where.getLimit() == null) {
            if (where.getAditional() != null) {
                return getLimit(where.getAditional());
            } else
                return null;
        } else {
            return where.getLimit();
        }
    }

    private Map<String, Object> setParam(Where where, Map<String, Object> value) {
        value.put(where.getField(), where.getValue());
        if (where.getAditional() == null || where.getAditional().getField() == null || where.getAditional().getField().isEmpty())
            return value;
        else {
            return setParam(where.getAditional(), value);
        }
    }

    private Predicate getPredicate(PathBuilder pathBuilder, Limit limit) {
        Predicate where = null;
        where = getPredicate(params, pathBuilder, where, limit);
        return where;
    }

    private Predicate getPredicate(Map<String, Object> map, PathBuilder pathBuilder, Predicate where, Limit limit) {
        for (Map.Entry<String, Object> v : map.entrySet()) {
            Object value = v.getValue();
            if (value instanceof String && ((String) value).startsWith(AlphabetFilter.STARTWITH)) {
                StringPath stringPath = pathBuilder.getString(v.getKey());
                String substring = ((String) value).substring(AlphabetFilter.STARTWITH.length());
                String lowerCase = substring.toLowerCase();
                where = where != null ? paramMode == ParamMode.AND ? stringPath.startsWith(substring).or(stringPath.startsWith(lowerCase)).and(where) : stringPath.startsWith(((String) value).substring(AlphabetFilter.STARTWITH.length() - 1)).
                        or(stringPath.startsWith(lowerCase)).or(where) :
                        stringPath.startsWith(substring).or(stringPath.startsWith(lowerCase));
                continue;
            }
            if (value instanceof RangeValue) {
                ComparablePath date = pathBuilder.getComparable(v.getKey(), ((RangeValue) value).getInicio().getClass());
                where = where != null ? paramMode == ParamMode.AND ? date.between((Comparable) ((RangeValue) value).getInicio(), (Comparable) ((RangeValue) value).getFin()).and(where) : date.between((Comparable) ((RangeValue) value).getInicio(), (Comparable) ((RangeValue) value).getFin()).or(where) : date.between((Comparable) ((RangeValue) value).getInicio(), (Comparable) ((RangeValue) value).getFin());
                continue;
            }
            if (value instanceof Collection) {
                Class type = pathBuilder.getType();
                Field field = ReflectionJavaUtil.getField(type, v.getKey());
                Class<?> klass = (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                Class aClass = field.getType();
                boolean assignableFrom = Arrays.asList(aClass.getInterfaces()).contains(Collection.class);
                if (assignableFrom) {
                    CollectionPath collection = pathBuilder.getCollection(v.getKey(), value.getClass());
                    if (collection != null) {
                        if (!(value instanceof Collection)) {
                            Object key = new Gson().fromJson((String) value, klass);
                            value = crudService.getById(klass, ReflectionJavaUtil.getIdValue(key));
                            where = where != null ? paramMode == ParamMode.AND ? collection.contains(value).and(where) : collection.contains(value).or(where) : collection.contains(value);
                        } else
                            for (Object val : (Collection) value) {
                                if (val == null) {
                                    where = where != null ? paramMode == ParamMode.AND ? collection.isEmpty().and(where) : collection.isEmpty().or(where) : collection.isEmpty();
                                } else {
                                    Object key = new Gson().fromJson((String) val, klass);
                                    long longValue = ((Long) ReflectionJavaUtil.getIdValue(key)).longValue();
                                    val = crudService.getEntityByID(klass, longValue);
                                    where = where != null ? paramMode == ParamMode.AND ? collection.contains(val).and(where) : collection.contains(val).or(where) : collection.contains(val);
                                }
                            }
                    }
                } else {
                    for (Object val : (Collection) value) {
                        Object key = new Gson().fromJson((String) val, klass);
                        val = crudService.getEntityByID(klass, ((Long) ReflectionJavaUtil.getIdValue(key)).longValue());
                        where = where != null ? pathBuilder.get(v.getKey()).eq(val).or(where) : pathBuilder.get(v.getKey()).eq(val);
                    }
                }
                continue;
            }
            if (value != null) {
                Object key = new Gson().fromJson((String) value, pathBuilder.getType());
                value = crudService.getEntityByID(pathBuilder.getType(), ((Long) ReflectionJavaUtil.getIdValue(key)).longValue());
                where = where != null ? paramMode == ParamMode.AND ? pathBuilder.get(v.getKey()).eq(value).and(where) : pathBuilder.get(v.getKey()).eq(value).or(where) : pathBuilder.get(v.getKey()).eq(value);
            }

        }
        return where;
    }
}
