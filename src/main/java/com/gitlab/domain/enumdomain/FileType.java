package com.gitlab.domain.enumdomain;

public enum FileType {
    MP4,
    WEBM,
    GIF,
    OTHER,
    PNG,
    AVI,
    JPG,
    MOV, JPEG
}
