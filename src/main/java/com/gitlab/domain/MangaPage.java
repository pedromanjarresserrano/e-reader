package com.gitlab.domain;

import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MangaPage extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private FileEntity pageFile;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FileEntity getPageFile() {
        return pageFile;
    }

    public void setPageFile(FileEntity pageFile) {
        this.pageFile = pageFile;
    }

}
