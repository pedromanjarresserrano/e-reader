package com.gitlab.domain;

import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.CrudOrderBy;
import com.gitlab.pedrioko.core.lang.annotation.ImageFileEntity;
import com.gitlab.pedrioko.core.lang.annotation.NoDuplicate;
import com.gitlab.pedrioko.core.view.api.ChosenItem;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@NoDuplicate(value = "name")
@Entity
@CrudOrderBy(value = "name")
public class Categoria extends BaseEntity implements ChosenItem {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @Lob
    @Column(length = 50000)
    private String name;
    @ImageFileEntity
    @OneToOne(cascade = CascadeType.ALL)
    private FileEntity picture;

    public Categoria() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }

    @Override
    public List<FileEntity> filesEntities() {
        return Arrays.asList(picture);
    }

    @Override
    public String visualName() {
        return name;
    }

    public FileEntity getPicture() {
        return picture;
    }

    public void setPicture(FileEntity picture) {
        this.picture = picture;
    }
}
