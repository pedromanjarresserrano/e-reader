package com.gitlab.domain;


import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.*;
import com.gitlab.pedrioko.core.view.api.ChosenItem;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;
import java.util.*;

@Entity
@CrudOrderBy(value = "nombre")
@NoDuplicate(value = "nombre")
@AlphabetSearch(field = "nombre")
public class Actriz extends BaseEntity implements ChosenItem {


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @CapitalizeFully
    @Column(unique = true)
    private String nombre;
    private String edad;
    @Lob
    @Column(name = "aka")
    private String alias;
    @ImageFileEntity
    @OneToOne(cascade = CascadeType.ALL)
    private FileEntity picture;
    @UseChosenFileEntity(orderBy = "nombre")
    @ManyToOne(cascade = CascadeType.ALL)
    private Genero gender;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<FileEntity> photosGallery;

    public Actriz() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public FileEntity getPicture() {
        return picture;
    }

    public void setPicture(FileEntity picture) {
        this.picture = picture;
    }

    public List<FileEntity> getPhotosGallery() {
        return new ArrayList<>(photosGallery);
    }

    public void setPhotosGallery(List<FileEntity> photosGallery) {
        this.photosGallery = new LinkedHashSet<>(photosGallery);
    }

    public Genero getGender() {
        return gender;
    }

    public void setGender(Genero gender) {
        this.gender = gender;
    }


    @Override
    public List<FileEntity> filesEntities() {
        return Arrays.asList(picture);
    }


    @Override
    public String visualName() {
        return getNombre();
    }


    @Override
    public String toString() {
        return nombre;
    }
}
