package com.gitlab.providers;

import com.gitlab.domain.SourcesLocation;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;

@Menu
public class SourcesLocationMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Sources Location");
    }

    @Override
    public Component getView() {
        CrudView crudView = new CrudView(SourcesLocation.class);
        return crudView;
    }

    @Override
    public String getIcon() {
        return "fas fa-video";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }
}
