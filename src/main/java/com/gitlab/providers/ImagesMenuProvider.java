package com.gitlab.providers;

import com.gitlab.domain.Image;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;

@Menu
public class ImagesMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Images");
    }

    @Override
    public Component getView() {
        CrudView crudView = new CrudView(Image.class);
        crudView.setPageSize(32);
        return crudView;
    }

    @Override
    public String getIcon() {
        return "fas fa-images";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }
}
