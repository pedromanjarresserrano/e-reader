package com.gitlab.providers;

import com.gitlab.domain.Categoria;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;

@Menu
public class CategoriasMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Categorias");
    }

    @Override
    public Component getView() {
        CrudView crudView = new CrudView(Categoria.class);
        crudView.setPageSize(30);
        return crudView;
    }

    @Override
    public String getIcon() {
        return "fas fa-list-ol";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }
}
