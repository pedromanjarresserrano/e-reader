package com.gitlab.providers;

import com.gitlab.domain.MangaSourceLocation;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;

@Menu
public class MangaSourcesLocationMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Manga Sources Location");
    }

    @Override
    public Component getView() {
        return new CrudView(MangaSourceLocation.class);
    }

    @Override
    public String getIcon() {
        return "fas fa-video";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }
}
