package com.gitlab.providers;

import com.gitlab.domain.Webm;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;

@Menu
public class WebmMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Webms ");
    }

    @Override
    public Component getView() {
        CrudView crudView = new CrudView(Webm.class);
        crudView.setPageSize(32);
        return crudView;
    }

    @Override
    public String getIcon() {
        return "fas fa-file-video";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }
}
