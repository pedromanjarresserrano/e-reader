package com.gitlab.providers;

import com.gitlab.domain.Actriz;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.ChosenItem;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.zk.component.chosenbox.ChosenBoxImage;
import com.gitlab.pedrioko.core.zk.component.colorpicker.ColorPicker;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.impl.ThreadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.LinkedHashMap;
import java.util.List;

@Menu
public class TestMenuProvider implements MenuProvider {

    @Autowired
    private ThreadServiceImpl taskExecutor;

    private static int totalCount = 0;

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Test Menu");
    }

    @Override
    public Component getView() {
        Div window = new Div();
        LinkedHashMap<Object, ChosenItem> value = new LinkedHashMap<>();
        List<Actriz> all = ApplicationContextUtils.getBean(CrudService.class).getAll(Actriz.class);
        all.forEach(e -> value.put(e.getId(), e));

        ChosenBoxImage chosenBoxImage = new ChosenBoxImage(value);
        window.appendChild(chosenBoxImage);
        Button child = new Button();

        window.appendChild(child);
        ColorPicker child1 = new ColorPicker();
        child.addEventListener(Events.ON_CLICK, e -> {
            System.out.println(child1.getValue());
        });
        window.appendChild(child1);
        Window window1 = new Window();
        window1.appendChild(window);
        return window1;
    }


    @Override
    public String getIcon() {
        return "fa fa-spinner spin";
    }

    @Override
    public int getPosition() {
        return 2;
    }

    @Override
    public String getGroup() {
        return "administracion";
    }
}
