package com.gitlab.actions;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ClipboardListener extends Thread implements ClipboardOwner {
	Clipboard sClip = Toolkit.getDefaultToolkit().getSystemClipboard();
	List<String> list = new ArrayList<>();
	Set<String> save = new LinkedHashSet<>();

	public void run() {
		Transferable trans = sClip.getContents(this);
		regainOwnership(trans);
		System.out.println("Listening to board...");
		while (true) {
		}
	}

	public void lostOwnership(Clipboard c, Transferable t) {
		try {
			this.sleep(1000); // this is for the program to wait until the large data is
								// copied into the clipboard. if it doesnt wait clipboard
		} // is empty by the time program looks for contents and returns
		catch (Exception e) // an exception.
		{
			e.printStackTrace();
		}
		Transferable contents = sClip.getContents(this); // EXCEPTION
		processContents(contents);
		regainOwnership(contents);
	}

	void processContents(Transferable t) {
		System.out.println("Processing: " + t);
		Transferable clipdata = sClip.getContents(this);
		String what;
		try {
			what = (String) (clipdata.getTransferData(DataFlavor.stringFlavor));

			list.add(what);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void regainOwnership(Transferable t) {
		sClip.setContents(t, this);
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

}