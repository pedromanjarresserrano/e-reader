package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.CrudMode;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import com.gitlab.pedrioko.services.CrudService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zul.Messagebox;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ToolAction
public class DeleteFileAction implements Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteFileAction.class);

    @Autowired
    private CrudService crudservice;

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public String getIcon() {
        return "fa fa-trash";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {
        CrudView crudViewParent = event.getCrudViewParent();
        Video value = (Video) event.getValue();
        if (value == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            try {
                Messagebox.show(ReflectionZKUtil.getLabel("eliminar") + " " + value.toString() + ", " + ReflectionZKUtil.getLabel("estaseguro"),
                        ReflectionZKUtil.getLabel("warning"), Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, e -> {
                            if (e.getName().equals("onOK")) {
                                String url = value.getUrl();
                                String replace = url.replace("\\", "\\\\");
                                File file = new File(replace);
                                try {
                                    FileUtils.forceDelete(file);
                                    file.getCanonicalFile().delete();
                                    Files.delete(Paths.get(replace));
                                } catch (NoSuchFileException e1) {
                                    //   Messagebox.show("No such file/directory exists");
                                } catch (DirectoryNotEmptyException e1) {
                                    Messagebox.show("Directory is not empty.");
                                } catch (IOException e1) {
                                    Messagebox.show("Invalid permissions.");
                                } catch (Exception ee) {
                                    Messagebox.show("Exception");
                                    ee.printStackTrace();
                                }
                              /*  if (file.delete()) {
                                    Messagebox.show("File deleted successfully");
                                } else {
                                    Messagebox.show("Failed to delete the file");
                                }*/
                                if (crudViewParent.getCrudviewmode() == CrudMode.MAINCRUD) {
                                    value.setActrices(new ArrayList<>());
                                    value.setCategorias(new ArrayList<>());

                                    crudservice.delete(crudservice.saveOrUpdate(value));
                                    crudViewParent.getCrudController().doQuery();
                                }
                                crudViewParent.update();
                                ZKUtil.showMessage(ReflectionZKUtil.getLabel("deleted"), MessageType.SUCCESS);
                            }
                        });
            } catch (Exception e) {
                LOGGER.error("ERROR on actionPerform()", e);
                ZKUtil.showMessage(ReflectionZKUtil.getLabel("errordelete"), MessageType.ERROR);
            }
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getClasses() {
        return "btn-danger";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.DELETE;
    }

    @Override
    public Integer position() {
        return Integer.MAX_VALUE;
    }

    @Override
    public String getColor() {
        return "#e74c3c";
    }

    @Override
    public int getGroup() {
        return 4;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Delete File");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
