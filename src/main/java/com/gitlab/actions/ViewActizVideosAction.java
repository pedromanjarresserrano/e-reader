package com.gitlab.actions;

import com.gitlab.domain.Actriz;
import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.api.ContentView;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.List;

@ToolAction
@Order(0)
public class ViewActizVideosAction implements Action {

    @Override
    public String getIcon() {
        return "fas fa-film";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        Actriz value = (Actriz) event.getValue();
        if (value == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            CrudView crudView = new CrudView(Video.class);
            crudView.addRootParams("actrices", Arrays.asList(value));
            String nombre = value.getNombre() + " Videos";
            crudView.getCrudController().doQuery();
            //   crudView.onlyEnable(Arrays.asList());
            ApplicationContextUtils.getBean(ContentView.class).addView(crudView, nombre, nombre);
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Actriz.class);
    }

    @Override
    public String getLabel() {
        return "View videos";
    }

    @Override
    public String getClasses() {
        return "btn btn-secondary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#000";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("View Videos");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
