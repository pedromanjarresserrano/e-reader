package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
public class GeneratePreviewsAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-info-circle";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {
        Video value = (Video) event.getValue();
        if (value == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            List<Video> all = crudService.getAll(Video.class);
            List<Video> videoStream = all.stream().map(e -> crudService.refresh(e)).filter(e -> e.getListfiles().isEmpty() || e.getListfiles().size() < 10).collect(Collectors.toList());
            videoStream.forEach(e -> {
                taskExecutor.execute(() -> {
                    e.setListfiles(videoService.generatePreviewImage(e.getUrl()));
                    crudService.save(e);
                });
            });
            // value.setListfiles(videoService.generatePreviewImage(value.getUrl()));
            //crudService.save(value);
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public String getClasses() {
        return "btn-info";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 3;
    }

    @Override
    public String getColor() {
        return "#FF98db";
    }

    @Override
    public int getGroup() {
        return 3;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("generar previews");
    }

    @Override
    public boolean isDefault() {
        return true;
    }
}
