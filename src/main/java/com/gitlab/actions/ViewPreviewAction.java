package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.api.ContentView;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.zk.component.gallery.Gallery;
import com.gitlab.pedrioko.core.zk.component.gallery.model.enums.GalleryType;
import org.springframework.core.annotation.Order;
import org.zkoss.zul.Window;

import java.util.Arrays;
import java.util.List;

//@ToolAction
@Order(0)
public class ViewPreviewAction implements Action {

    @Override
    public String getIcon() {
        return "fas fa-play";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        Video value = (Video) event.getValue();
        if (value == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            Window w = new Window();
            w.setContentStyle("background: black;");
            w.setStyle("background: black;");
            w.setHeight("100%");
            Gallery gallery = new Gallery();
            gallery.setGalleryType(GalleryType.NATURAL);
            w.appendChild(gallery);
            gallery.setHeight("500px");
            ApplicationContextUtils.getBean(ContentView.class).addView(w, value.name(), value.name());
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "ViewPreviewAction";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#FFFF00";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("ViewPreviewAction");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
