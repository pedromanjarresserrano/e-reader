package com.gitlab.actions;

import com.gitlab.domain.Image;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.api.ContentView;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.zk.component.gallery.Gallery;
import com.gitlab.pedrioko.core.zk.component.gallery.model.GalleryItem;
import com.gitlab.pedrioko.core.zk.component.gallery.model.enums.GalleryType;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.zkoss.zul.Window;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
@Order(0)
public class ViewImagesAction implements Action {
    @Autowired
    private StorageService storageService;
    @Autowired
    private CrudService crudService;

    @Override
    public String getIcon() {
        return "fas fa-play";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        List<Image> value = crudService.getAllOrderBy(Image.class,"name");
        if (value == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            Window w = new Window();
            w.setContentStyle("background: black;");
            w.setStyle("background: black;");
            w.setHeight("100%");
            Gallery gallery = new Gallery();
            gallery.setGalleryType(GalleryType.NATURAL);
            gallery.setGalleryItems(value.stream().map(e -> {
                GalleryItem galleryItem = new GalleryItem();
                String urlFile = storageService.getUrlFile(e.getName());
                String enlargedSrc = "/statics/files" + e.getShorturl().replace("\\", "/");
                galleryItem.setEnlargedSrc(enlargedSrc);
                galleryItem.setThumbnailSrc(enlargedSrc);
                return galleryItem;
            }).collect(Collectors.toList()));
            w.appendChild(gallery);
            gallery.setHeight("100%");
            ApplicationContextUtils.getBean(ContentView.class).addView(w, "IMAGES-VIEWER", "Images");
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Image.class);
    }

    @Override
    public String getLabel() {
        return "View images";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#00FFFF";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("ViewPreviewAction");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
