package com.gitlab.actions;

import com.gitlab.domain.SourcesLocation;
import com.gitlab.domain.Webm;
import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ThreadService;
import com.gitlab.pedrioko.services.VideoService;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
@Order(0)
public class ProcessWebmLocationAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private ThreadService taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-plus";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        SourcesLocation location = (SourcesLocation) event.getValue();
        if (location == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            String value = location.getDireccion();
            if (value != null && !value.isEmpty()) {
                try {

                    List<Path> list2 = Files.walk(Paths.get(value), Integer.MAX_VALUE).parallel()
                            .filter(path -> !path.toFile().isDirectory())
                            .collect(Collectors.toList());

                    for (int i = 0; i < list2.size(); i++) {
                        int finalI = i;
                        taskExecutor.addProcess(() -> {
                            try {
                                Path e = list2.get(finalI);
                                String mime = Files.probeContentType(e);
                                String name1 = e.getFileName().toString();
                                taskExecutor.setCurrent(name1);

                                if (mime.equalsIgnoreCase("video/webm")) {
                                    Webm webm = new Webm();
                                    webm.setName(name1);

                                    String absolutePath = e.toFile().getAbsolutePath();
                                    PathBuilder<?> pathBuilder = crudService.getPathBuilder(Webm.class);
                                    StringPath name = pathBuilder.getString("name");
                                    List<?> fetch = crudService.query().from(pathBuilder).where(name.eq(webm.name())).fetch();
                                    if (fetch != null && fetch.isEmpty()) {
                                            webm.setListfiles(videoService.generatePreviewImage(absolutePath));
                                    } else {
                                        webm = crudService.refresh((Webm) fetch.get(0));
                                        if (webm.getListfiles() == null || webm.getListfiles().isEmpty() || webm.getListfiles().size() < 10)
                                            if (webm.getTipo() == null) {
                                                webm.addAll(videoService.generatePreviewImage(absolutePath));
                                            } else {
                                                webm.addAll(videoService.generatePreviewImage(absolutePath));
                                            }
                                    }
                                    if (webm.getSize() == null || webm.getSize() == 0) {
                                        webm.setSize(e.toFile().length());
                                    }
                                    if (webm.getDuration() == null || webm.getDuration() == 0) {
                                        webm.setDuration(videoService.getTime(absolutePath));
                                    }

                                    if (webm.getSize() == null || webm.getSize() == 0) {
                                        webm.setSize(e.toFile().length());
                                    }

                                    if (webm.getUploadDate() == null)
                                        webm.setUploadDate(new Date());

                                    if (webm.getTipo() == null) {
                                        switch (mime) {
                                            case "video/webm":
                                                webm.setTipo(FileType.WEBM);
                                                break;
                                            default:
                                                webm.setTipo(FileType.OTHER);
                                                break;
                                        }
                                    }
                                    if (webm.getShorturl() == null || webm.getShorturl().isEmpty()) {
                                        webm.setShorturl(absolutePath.replace(value, ""));
                                    }
                                    webm.setUrl(absolutePath);
                                    crudService.saveOrUpdate(webm);
                                }
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        });

                    }
                } catch (Exception w) {
                    w.printStackTrace();
                }
            }
        }

    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(SourcesLocation.class);
    }

    @Override
    public String getLabel() {
        return "Process Webm Location";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#FF00FF";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Process Webm Location");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
