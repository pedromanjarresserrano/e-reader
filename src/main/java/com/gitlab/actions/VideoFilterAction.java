package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolFilter;
import com.gitlab.pedrioko.core.view.api.ToolbarFilter;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import com.gitlab.pedrioko.core.zk.component.rangebox.DurationRangeBox;
import org.zkoss.zk.ui.Component;

import java.util.Arrays;
import java.util.List;

@ToolFilter
public class VideoFilterAction implements ToolbarFilter {
    private DurationRangeBox durationRangeBox = new DurationRangeBox();

    @Override
    public String getLabel() {
        return "Label";
    }

    @Override
    public String getField() {
        return "duration";
    }

    @Override
    public String getTooltipText() {
        return "TooltipText";
    }

    @Override
    public Component getComponent() {
        return durationRangeBox;
    }

    @Override
    public void onChangeAction(CrudView crudView) {
        crudView.getCrudController().put(getField(), durationRangeBox.getValue());
    }

    @Override
    public List<Class<?>> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public Integer position() {
        return 0;
    }
}
