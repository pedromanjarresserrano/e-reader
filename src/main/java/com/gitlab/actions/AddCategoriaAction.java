package com.gitlab.actions;

import com.gitlab.domain.Categoria;
import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.view.forms.CustomForm;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.view.util.ArraysUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import com.gitlab.pedrioko.core.zk.component.chosenbox.ChosenFileEntityBox;
import com.gitlab.pedrioko.services.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

@ToolAction
@Order(0)
public class AddCategoriaAction implements Action {

    @Autowired
    private CrudService crudService;
    private static final String AGREGAR = "Agregar";

    @Override
    public String getIcon() {
        return "fa  fa-list";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        Video video = (Video) crudService.refresh(event.getValue());
        if (video == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            CustomForm form = new CustomForm(Categoria.class, new LinkedHashMap<>());
            form.addField(AGREGAR, ChosenFileEntityBox.class);
            ChosenFileEntityBox combobox = (ChosenFileEntityBox) form.getComponentField(AGREGAR);
            List<? extends Object> all = ApplicationContextUtils.getBean(CrudService.class).getAllOrderBy(Categoria.class, "name");
            List<Categoria> value = video.getCategorias();
            ArraysUtil.removeDuplicates(Categoria.class, all, value);
            combobox.setModel(all);
            form.getRenglon(AGREGAR).setZclass("col-md-12 col-lg-12 col-xs-12 col-sm-12");
            form.addAction(ReflectionZKUtil.getLabel("agregar"), "fa fa-plus", e -> {
                Collection<? extends Categoria> valueSelection = (Collection<? extends Categoria>) combobox.getValueSelection();
                List<Categoria> categorias = video.getCategorias();
                categorias.addAll(valueSelection);
                video.setCategorias(categorias);
                crudService.saveOrUpdate(video);
                form.detach();
                ZKUtil.showMessage(ReflectionZKUtil.getLabel("userbasicform.guardar"), MessageType.SUCCESS);
                CrudView crudViewParent = event.getCrudViewParent();
                if (crudViewParent != null) crudViewParent.update();
            });
            form.addAction(ReflectionZKUtil.getLabel("cancelar"), "fa fa-ban", "btn btn-danger", e -> form.detach());
            form.setTitle(AGREGAR);
            ZKUtil.showDialogWindow(form);
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "Add Categorias";
    }

    @Override
    public String getClasses() {
        return "btn-info";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#0099aa";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Agregar categorias");
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }

    @Override
    public boolean showLabel() {
        return true;
    }
}
