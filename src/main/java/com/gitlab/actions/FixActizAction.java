package com.gitlab.actions;

import com.gitlab.domain.Actriz;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@ToolAction
@Order(0)
public class FixActizAction implements Action {
    @Autowired
    private CrudService crudService;

    @Override
    public String getIcon() {
        return "fas fa-settings";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {
        List<Actriz> value = crudService.getAll(Actriz.class);
        value.forEach(e -> {
            if (e.getCreatedAt() == null) e.setCreatedAt(new Date());
            crudService.save(e);
        });
        event.getCrudViewParent().getCrudController().doQuery();
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Actriz.class);
    }

    @Override
    public String getLabel() {
        return "Fix create Date";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#ff00ff";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Fix Action");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
