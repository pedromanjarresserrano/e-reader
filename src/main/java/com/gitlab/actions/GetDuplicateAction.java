package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.math.BigDecimal;
import java.util.*;

@ToolAction
@Order(0)
public class GetDuplicateAction implements Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetDuplicateAction.class);

    @Autowired
    private CrudService crudService;

    @Override
    public String getIcon() {
        return "fas fa-play";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        PathBuilder<?> pathBuilder = crudService.getPathBuilder(Video.class);
        NumberPath<Double> durationPath = pathBuilder.getNumber("duration", Double.class);
        JPAQuery<Double> query = crudService.query().select(durationPath)
                .from(pathBuilder)
                .groupBy(durationPath)
                .having(durationPath.count().gt(BigDecimal.ONE));
        List<Double> duration = query.fetch();
        System.out.println(duration);
        event.getCrudViewParent().getCrudController().putRoot("duration", duration);
        event.getCrudViewParent().getCrudController().doQuery();
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "Play";
    }

    @Override
    public String getClasses() {
        return "btn-danger";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Play");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
